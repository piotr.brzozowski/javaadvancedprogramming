package factory_method.v1;

public enum ChocolateType {
    MILK("milk"), NUTS("nuts"), DARK("dark"), STRAWBERRY("strawberry");
    private String choclateType;

    ChocolateType(String choclateType) {
        this.choclateType = choclateType;
    }
}