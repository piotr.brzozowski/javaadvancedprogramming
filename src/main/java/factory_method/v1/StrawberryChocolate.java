package factory_method.v1;

public class StrawberryChocolate extends Chocolate {
    public StrawberryChocolate() {
        super(ChocolateType.STRAWBERRY);
    }

    @Override
    public String getDescription() {
        return "Czekolada z truskawkami";
    }

    private void showStrawberryInfo(int percentage) {
        System.out.println(String.format("Czekolada zawiera %d%% truskawi", percentage));
    }

    @Override
    public void showChocolate() {
        showCacaoInfo(50);
        showStrawberryInfo(30);
        showSugarInfo(20);
    }
}