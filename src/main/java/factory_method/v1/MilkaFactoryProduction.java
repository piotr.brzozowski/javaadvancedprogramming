package factory_method.v1;

import java.util.Scanner;

public class MilkaFactoryProduction {
    public static void main(String[] args) {
        System.out.println("Wybierz fabryke: 1 -> Wedel, 2 -> Milka");
        Scanner input = new Scanner(System.in);
        int factoryType = input.nextInt();
        ChocolateFactory factory = null;
        if (factoryType == 1) {
            System.out.println("Fabryka Wedla");
            factory = new WedelFactory();
        }else if (factoryType == 2) {
            System.out.println("Fabryka Milka");
            factory = new MilkaFactory();
        }
        chocolateProducer(factory);
    }

    public static void chocolateProducer(ChocolateFactory chocolateFactory) {
        Chocolate chocolate = chocolateFactory.produceChocolate(ChocolateType.STRAWBERRY);
        System.out.println(String.format("opis: %s", chocolate.getDescription()));
        chocolate.showChocolate();
    }
}