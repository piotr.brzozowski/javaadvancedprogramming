package factory_method.v1;

public class WedelFactory implements ChocolateFactory {

    @Override
    public Chocolate produceChocolate(ChocolateType type) {
        Chocolate chocolate = null;
        switch (type) {
            case DARK:
                chocolate = new DarkChocolate();
                break;
            case MILK:
                chocolate = new MilkChocolate();
                break;
            case NUTS:
                chocolate = new NutsChocolate();
                break;
            case STRAWBERRY:
                chocolate = new StrawberryChocolate();
                break;
        }
        return chocolate;
    }
}