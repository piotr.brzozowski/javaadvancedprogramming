package factory_method.v1;

public interface ChocolateFactory {
    Chocolate produceChocolate(ChocolateType type);
}