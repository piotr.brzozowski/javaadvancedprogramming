package factory_method.v2;

//ConcreteProduct z diagramu UML
public class OsxDialog extends SystemDialog {
    public OsxDialog() {
        super(DialogType.OSX);
    }

    @Override
    public void show(String message) {
        System.out.println("@@@@@@@@Osx@@@@@@@@");
        System.out.println("@@@@@@@@___@@@@@@@@");
        System.out.println(message);
        System.out.println("@@@@@@@@___@@@@@@@@");
        System.out.println("@@@@@@@@___@@@@@@@@");
        System.out.println("@@@@@@@@Osx@@@@@@@@");
    }
}
