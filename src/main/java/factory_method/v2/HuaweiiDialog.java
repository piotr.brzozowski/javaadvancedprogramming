package factory_method.v2;

public class HuaweiiDialog extends SystemDialog{
    public HuaweiiDialog() {
        super(DialogType.HUAWEII);
    }

    @Override
    public void show(String message) {
        System.out.println("Huaweii dialog: " + message);
    }
}
