package factory_method.v2;

public class OperationSystem {

    public static void main(String[] args) {
        DialogFactory dialogFactory = new SystemDialogFactory();
        SystemDialog systemDialog1 = dialogFactory.create(DialogType.HUAWEII);
        systemDialog1.show("First message");
        System.out.println("-----------------------------");
        SystemDialog systemDialog2 = dialogFactory.create(DialogType.HUAWEII);
        systemDialog2.show("Second message");
    }
}
