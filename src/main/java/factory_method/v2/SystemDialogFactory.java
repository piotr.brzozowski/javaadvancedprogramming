package factory_method.v2;

import java.util.NoSuchElementException;

//ConcreteFactory z diagramu UML
public class SystemDialogFactory implements DialogFactory {

    @Override
    public SystemDialog create(DialogType dialogType) {
        SystemDialog systemDialog = null;
        for (DialogType type : DialogType.values()) {
            if (type.equals(dialogType)){
                systemDialog = type.dialogCreator.create();
            }
        }
        return systemDialog;
//        switch (dialogType) {
//            case OSX:
//                systemDialog = new OsxDialog();
//                break;
//            case LINUX:
//                systemDialog = new LinuxDialog();
//                break;
//            case ANDROID:
//                systemDialog = new AndroidDialog();
//                break;
//            case WINDOWS:
//                systemDialog = new WindowsDialog();
//                break;
//            case HUAWEII:
//                systemDialog = new HuaweiiDialog();
//                break;
//            default:
//                throw new NoSuchElementException("No dialog type supported!");
//        }
//        return systemDialog;
    }
}
