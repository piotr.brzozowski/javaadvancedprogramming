package factory_method.v2;

//ConcreteProduct z diagramu UML
public class LinuxDialog extends SystemDialog {
    public LinuxDialog() {
        super(DialogType.LINUX);
    }

    @Override
    public void show(String message) {
        System.out.println("********Linux********");
        System.out.println("********_____********");
        System.out.println(message);
        System.out.println("********_____********");
        System.out.println("********_____********");
        System.out.println("********Linux********");
    }
}
