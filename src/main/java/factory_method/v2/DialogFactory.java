package factory_method.v2;

//Factory z diagramu UML
public interface DialogFactory {
    SystemDialog create(DialogType dialogType);
}
