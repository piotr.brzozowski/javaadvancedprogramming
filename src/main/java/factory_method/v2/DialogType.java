package factory_method.v2;

public enum DialogType {
    OSX("osx", new DialogCreator() {
        @Override
        public SystemDialog create() {
            return new OsxDialog();
        }
    }), WINDOWS("windows", new DialogCreator() {
        @Override
        public SystemDialog create() {
            return new WindowsDialog();
        }
    }), LINUX("linux", new DialogCreator() {
        @Override
        public SystemDialog create() {
            return new LinuxDialog();
        }
    }), ANDROID("android", new DialogCreator() {
        @Override
        public SystemDialog create() {
            return new AndroidDialog();
        }
    }), HUAWEII("huaweii", new DialogCreator() {
        @Override
        public SystemDialog create() {
            return new HuaweiiDialog();
        }
    });
    public String type;
    public DialogCreator dialogCreator;

    DialogType(String type, DialogCreator dialogCreator) {
        this.type = type;
        this.dialogCreator = dialogCreator;
    }
}
