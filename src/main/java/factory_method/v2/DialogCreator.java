package factory_method.v2;

public interface DialogCreator {

    SystemDialog create();
}
