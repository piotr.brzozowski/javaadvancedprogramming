package factory_method.v2;

//ConcreteProduct z diagramu UML
public class AndroidDialog extends SystemDialog{

    public AndroidDialog(){
        super(DialogType.ANDROID);
    }

    @Override
    public void show(String message) {
        System.out.println("!!!!!!!!Android!!!!!!!!");
        System.out.println("!!!!!!!!_______!!!!!!!!");
        System.out.println(message);
        System.out.println("!!!!!!!!_______!!!!!!!!");
        System.out.println("!!!!!!!!_______!!!!!!!!");
        System.out.println("!!!!!!!!Android!!!!!!!!");
    }
}
