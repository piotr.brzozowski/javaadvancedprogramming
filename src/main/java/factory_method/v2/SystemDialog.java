package factory_method.v2;

//Product z diagramu UML
public abstract class SystemDialog {
    private DialogType dialogType;

    public SystemDialog(DialogType dialogType) {
        this.dialogType = dialogType;
    }


    public abstract void show(String message);

}
