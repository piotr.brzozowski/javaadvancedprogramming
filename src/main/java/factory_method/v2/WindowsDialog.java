package factory_method.v2;

//ConcreteProduct z diagramu UML
public class WindowsDialog extends SystemDialog {

    public WindowsDialog() {
        super(DialogType.WINDOWS);
    }

    @Override
    public void show(String message) {
        System.out.println("########Windows##########");
        System.out.println("########_______##########");
        System.out.println(message);
        System.out.println("########_______##########");
        System.out.println("########_______##########");
        System.out.println("########Windows##########");
    }
}
