package java_8.optional;

import java.util.Optional;
import java.util.Random;

public class OptionalExample {

    public static void main(String[] args) {
        review();
        String text = null;
        Optional<String> optional = Optional.of(text);
        //get -> niebezpieczne wywołanie
        //System.out.println(optional.get());
        optional.ifPresent((String value) -> {
            System.out.println("Value from optional: " + value);
        });
        String valueFromOptional = optional.orElse("UNKNOWN");
        System.out.println("Value from optiona: " + valueFromOptional);


        Integer value = null;
        Optional<Integer> magicNumber = Optional.ofNullable(value);

        int intValueFromOptional = magicNumber.orElseGet(()->{
            Random random = new Random();
            int v1 = random.nextInt(100);
            int v2 = random.nextInt(200);
            return v1 + v2;
        });
        System.out.println("Int value from optiona: " + intValueFromOptional);

        int magicValueFromOptional = magicNumber.orElseThrow(()->{
            return new IllegalArgumentException("can't find magic number!");
        });
        System.out.println(magicValueFromOptional);

        System.out.println("Koniec");
    }

    public static void review() {
        String text = null;
        //Optional<String> optText = Optional.of(text); //  -> zostanie wyrzucony wyjątek NullPointerException
        Optional<String> optText = Optional.ofNullable(text); //tworzenie optional z założeniem, że może zawierać null
        //String valueFromOpt = optText.get(); // -> zostanie wyrzucony wyjątek NoSuchElementException
        optText.ifPresent((String textFromOptional) -> {
            System.out.println("Text from opt: " + textFromOptional);
        });
        String textFromOptional = optText.orElse("UNKNOWN");
        String textFromOptionalV2 = optText.orElseGet(()->{
            String t1 = String.valueOf(System.currentTimeMillis());
            String t2 = "test";
            return t1 + t2;
        });
        String textFromOptionalV3 = optText.orElseThrow(()->{
            return new IllegalArgumentException("no value in optional");
        });
    }
}
