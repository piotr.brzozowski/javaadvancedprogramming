package java_8.example;

public class ExamplePlayground {

    public static void main(String[] args) {
        Calculator calculator = (int x, int y) -> x + y;
        System.out.println("Calculator example: ");
        System.out.println(calculator.calculate(10, 20));

        Converter converter = () -> "Converter example";
        System.out.println("Converter -> ");
        System.out.println(converter.convert());

        Downloader downloader = () -> {
            System.out.println("Download image in 4K resolution");
            System.out.println(".....");
            System.out.println(".....");
        };
        System.out.println("Download -> ");
        downloader.download();

        TextOperation textOperation1 = (message) -> System.out.println(message.toUpperCase());
        System.out.println("Text operation -> ");

        updateText(textOperation1, 99);
        updateText((message)->{
            System.out.println(message.charAt(0));
        }, 78);
        updateText((message)->{
            String newMessage = message + "ABCD";
            System.out.println(newMessage);
        }, 21);

        updateText((message)->{
            String newMessage = message.replace("1", "X");
            System.out.println(newMessage);
        }, 1234);
    }


    public static void updateText(TextOperation textOperation, int operationId) {
        String randomValue = String.valueOf(System.currentTimeMillis());
        System.out.println("Start processing.... with id:" + operationId);
        //do process
        textOperation.invoke(randomValue);//wykonujemy dowolną operację zdefiniowaną w momencie wywołania metody
        System.out.println("Finish processing....");
    }
}
