package java_8.example;

interface TextOperation {
		void invoke(String text);
}