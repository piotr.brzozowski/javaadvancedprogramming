package java_8.example;

interface Calculator {
	int calculate(int a, int b);
}