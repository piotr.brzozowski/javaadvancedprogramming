package java_8.review;

public interface Executor {

    int execute(int a, String name);

    public static void main(String[] args) {
        Executor executorOld = new Executor() {
            @Override
            public int execute(int a, String name) {
                return a + Integer.valueOf(name);
            }
        };

        Executor executor = (int param, String str) -> {
            return param + Integer.valueOf(str);
        };


    }

}
/*
(param, str) -> {
    return param + Integer.valueOf(str);
}
 */
