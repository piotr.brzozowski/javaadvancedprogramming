package java_8;

public class AddMathOperation implements MathOperation {
    @Override
    public int calculate(int a, int b) {
        return a + b;
    }
}
