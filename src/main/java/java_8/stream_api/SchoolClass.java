package java_8.stream_api;

import java.util.List;

public class SchoolClass {

    private String name;
    private int level;
    private List<Student> students;

    public SchoolClass(String name, int level, List<Student> students) {
        this.level = level;
        this.name = name;
        this.students = students;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Student> getStudents() {
        return students;
    }

    public void setStudents(List<Student> students) {
        this.students = students;
    }
}
