package java_8.stream_api;

import java.util.ArrayList;
import java.util.List;

public class Student {
    private int id;
    private String name;
    private String lastName;
    private int yearOfStudies;
    private Course course;
    private List<Integer> scores = new ArrayList<>();

    public Student(int id, String name, String lastName, int yearOfStudies, Course course, List<Integer> scores) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.yearOfStudies = yearOfStudies;
        this.course = course;
        this.scores = scores;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getYearOfStudies() {
        return yearOfStudies;
    }

    public void setYearOfStudies(int yearOfStudies) {
        this.yearOfStudies = yearOfStudies;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public List<Integer> getScores() {
        return scores;
    }

    public void setScores(List<Integer> scores) {
        this.scores = scores;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", yearOfStudies=" + yearOfStudies +
                ", course=" + course +
                ", scores=" + scores +
                '}';
    }
}