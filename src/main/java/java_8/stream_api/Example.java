package java_8.stream_api;

import java.util.Arrays;
import java.util.List;

public class Example {

    public static void main(String[] args) {
        List<String> names = Arrays.asList("Piotr", "Wacław", "Ignacy");
        names.stream().forEach((String name)->{
            System.out.println("NAME: ");
            System.out.println(name);
        });
        names.stream().forEach((String name) -> {
            System.out.println("***************");
            System.out.println(name);
            System.out.println("***************");
        });

        names.stream().map((String name)->{
            return name.toUpperCase();
        }).forEach((String name) -> {
            System.out.println("NAME: " + name);
        });

        names.stream().map((String name) -> {
            return name.toUpperCase();
        }).map((String name) -> {
            return name.length();
        }).forEach((Integer nameLength) -> {
            System.out.println(nameLength);
        });

        //chcemy znaleść długość imion zaczynające się na literę P lub W o parzystej długości

        names.stream()
                .filter((String name)->name.length() % 2 == 0)
                .filter((String name) -> name.startsWith("P") || name.startsWith("W"))
                .map((String name) -> new Pair(name, name.length()))
                .forEach(
                        (Pair data) -> System.out.println(data.name + " -> " + data.length)
                );
    }
}

class Pair {
    String name;
    int length;

    public Pair(String name, int length) {
        this.name = name;
        this.length = length;
    }
}
