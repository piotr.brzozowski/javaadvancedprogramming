package java_8.stream_api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class StudentExample {

    public static void main(String[] args) {
        List<Student> students = Arrays.asList(
                new Student(1, "John", "Cool", 3, Course.IT, Arrays.asList(3, 4, 5, 2)),
                new Student(2, "Sam", "Smith", 2, Course.BIOLOGY, Arrays.asList(3, 5, 6, 4, 2)),
                new Student(3, "Caroline", "Con", 4, Course.MATH, Arrays.asList(5, 5, 5, 5)),
                new Student(4, "Jan", "Nowak", 1, Course.IT, Arrays.asList(3, 4, 4, 2))
        );

        System.out.println("znaleść id studentow, którzy mają conajmniej jedną ocene 2 w dzienniku");
        students.stream()
                .filter((Student student) -> student.getScores().contains(2))
                .map((Student student) -> student.getId())
                .forEach((Integer id) -> System.out.println(id));

        System.out.println("znaleść id studentów którzy mają conajmniej jedną ocene 2 w dzienniku i studiują Matematykę");
        students.stream()
                .filter((Student student) -> student.getScores().contains(2))
                .filter((Student student) -> student.getCourse() == Course.MATH)
                .map((Student student) -> student.getId())
                .forEach((Integer id) -> System.out.println(id));

        System.out.println("obliczyć średnią ocen każdego studenta IT");
//        students.stream()
//                .filter(new Predicate<Student>() {
//                    @Override
//                    public boolean test(Student student) {
//                        return student.getCourse() == Course.IT;
//                    }
//                })
//                .map(new Function<Student, Integer>() {
//                    @Override
//                    public Integer apply(Student student) {
//                        int average = 0;
//                        for (int score : student.getScores()) {
//                            average += score;
//                        }
//                        return average / student.getScores().size();
//                    }
//                }).forEach(new Consumer<Integer>() {
//            @Override
//            public void accept(Integer integer) {
//                System.out.println(integer);
//            }
//        });
        students.stream()
                .filter((Student student) -> student.getCourse() == Course.IT)
                .map((Student student) -> {
                    int average = 0;
                    for (int score : student.getScores()) {
                        average += score;
                    }
                    return average / student.getScores().size();
                }).forEach((Integer average) -> System.out.println(average));

        System.out.println("Znaleść imiona studentów, którzy będąc na conajmniej 3 roku studiów, mają conajmniej jednką 5");
        students.stream()
                .filter((Student student) -> student.getYearOfStudies() >= 3)
                .filter((Student student) -> student.getScores().contains(5))
                .map((Student student) -> student.getName())
                .forEach((String name) -> System.out.println("Name: " + name));

        List<String> names = students.stream()
                .filter((Student student) -> student.getYearOfStudies() >= 3)
                .filter((Student student) -> student.getScores().contains(5))
                .map((Student student) -> student.getName())
                .collect(Collectors.toList());
        System.out.println("Names: " + names);

        students.stream()
                .filter((Student s) -> s.getScores().contains(2))
                .findAny()
                .ifPresent((Student s) -> {
                    System.out.println("Student with score 2: (findAny)" + s);
                });

        Student student = students.stream()
                .filter((Student s) -> s.getScores().contains(2))
                .findFirst()
                .orElseGet(() -> {
                    return new Student(-1, "unknown", "unknown", -1, Course.IT, new ArrayList<>());
                });
        System.out.println("Student with score 2: (findFirst)" + student);

    }
}
