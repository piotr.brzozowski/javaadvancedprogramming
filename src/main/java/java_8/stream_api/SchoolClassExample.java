package java_8.stream_api;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class SchoolClassExample {

    public static void main(String[] args) {
        SchoolClass schoolClass1A = new SchoolClass("A", 1,
                Arrays.asList(
                        new Student(1, "John", "Cool", 3, Course.IT, Arrays.asList(3, 4, 5, 2)),
                        new Student(2, "Sam", "Smith", 2, Course.BIOLOGY, Arrays.asList(3, 5, 6, 4, 2))
                )
        );
        SchoolClass schoolClass1B = new SchoolClass("B", 1, Arrays.asList(
                new Student(3, "Caroline", "Con", 4, Course.MATH, Arrays.asList(5, 5, 5, 5)),
                new Student(4, "Jan", "Nowak", 1, Course.IT, Arrays.asList(3, 4, 4, 2))
        ));
        SchoolClass schoolClass2C = new SchoolClass("C", 2, Arrays.asList(
                new Student(3, "Wacław", "Kowalki", 4, Course.MATH, Arrays.asList(5, 5, 5, 5)),
                new Student(4, "Joanna", "Nowak", 1, Course.IT, Arrays.asList(3, 4, 4, 2))
        ));

        List<SchoolClass> schoolClasses = Arrays.asList(schoolClass1A, schoolClass1B, schoolClass2C);

        System.out.println("Strumień list studentów");
        schoolClasses.stream()
                .map((SchoolClass schoolClass) -> {
                    return schoolClass.getStudents();
                })
                .forEach((List<Student> classStudents) -> {
                    System.out.println(classStudents);
                });

        System.out.println("Strumień wszystkich studentów");
        schoolClasses.stream()
                .flatMap((SchoolClass schoolClass) -> {
                    return schoolClass.getStudents().stream();
                })
                .forEach((Student student) -> {
                    System.out.println(student);
                });

        System.out.println("Imiona studentów klas pierwszych z oceną 2 w dzienniku:");

        List<String> names = schoolClasses.stream()
                .filter((SchoolClass schoolClass) -> {
                    return schoolClass.getLevel() == 1;
                })
                .flatMap((SchoolClass schoolClass) -> {
                    return schoolClass.getStudents().stream();
                })
                .filter((Student student) -> {
                    return student.getScores().contains(2);
                })
                .map((Student student) -> {
                    return student.getName();
                })
                .collect(Collectors.toList());

        System.out.println(names);

        System.out.println("List wszystkich ocen: ");
        List<Integer> schoolScores = schoolClasses.stream()
                .flatMap((SchoolClass schoolClass) -> {
                    return schoolClass.getStudents().stream();
                })
                .flatMap((Student student) -> {
                    return student.getScores().stream();
                })
                .collect(Collectors.toList());
        System.out.println(schoolScores);
    }
}
