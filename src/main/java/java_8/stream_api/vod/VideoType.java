package java_8.stream_api.vod;

enum VideoType {
    CLIP, PREVIEW, EPISODE
}