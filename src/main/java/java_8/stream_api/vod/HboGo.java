package java_8.stream_api.vod;

import java.util.*;
import java.util.stream.Collectors;

public class HboGo {

    public static void main(String[] args) {
        Video video = new Video("GOT1", "got1.com", VideoType.CLIP);
        Video video1 = new Video("GOT2", "got2.com", VideoType.EPISODE);
        Video video2 = new Video("GOT3", "got3.com", VideoType.PREVIEW);
        Video video3 = new Video("GOT4", "got4.com", VideoType.PREVIEW);
        Video video4 = new Video("GOT5", "got5.com", VideoType.CLIP);
        Video video5 = new Video("GOT6", "got6.com", VideoType.EPISODE);

        Episode episode = new Episode("got1", 1,
                Arrays.asList(video, video1));
        Episode episode1 = new Episode("got1", 1,
                Arrays.asList(video, video1));
        Episode episode2 = new Episode("got2", 2,
                Arrays.asList(video2, video4));
        Episode episode3 = new Episode("got3", 1,
                Arrays.asList(video3, video5));

        Season season = new Season("GOTS1", 1,
                Arrays.asList(episode, episode1));

        Season season1 = new Season("GOTS1", 1,
                Arrays.asList(episode2));

        Season season2 = new Season("GOTS1", 2,
                Arrays.asList(episode2, episode3));


        List<Season> seasons = Arrays.asList(season, season1, season2);

        System.out.println("Lista wszystkich epizodów: ");
        List<Episode> episodes = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .collect(Collectors.toList());
        System.out.println(episodes);

        System.out.println("Lista wszystkich video: ");
        List<Video> videos = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .collect(Collectors.toList());
        System.out.println(videos);

        System.out.println("Lista nazw sezonów: ");
        List<String> seasonNames = seasons.stream()
                .map((Season s) -> s.seasonName)
                .collect(Collectors.toList());
        System.out.println(seasonNames);

        System.out.println("Lista numerów sezonów: ");
        List<Integer> seasonNumbers = seasons.stream()
                .map((Season s) -> s.seasonNumber)
                .collect(Collectors.toList());
        System.out.println(seasonNumbers);

        System.out.println("Lista nazw epizodów: ");
        List<String> episodeNames = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .map((Episode e) -> e.episodeName)
                .collect(Collectors.toList());
        System.out.println(episodeNames);

        System.out.println("Lista numerów epizodów: ");
        List<Integer> episodeNumbers = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .map((Episode e) -> e.episodeNumber)
                .collect(Collectors.toList());
        System.out.println(episodeNumbers);

        System.out.println("Lista nazw video: ");
        List<String> videoNames = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .map((Video v) -> v.title)
                .collect(Collectors.toList());
        System.out.println(videoNames);

        System.out.println("Lista url video: ");
        List<String> videoUrls = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .map((Video v) -> v.url)
                .collect(Collectors.toList());
        System.out.println(videoUrls);

        System.out.println("Lista epizodów z parzystych sezonów: ");
        List<Episode> seasonEpisodes = seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .collect(Collectors.toList());
        System.out.println(seasonEpisodes);

        System.out.println("Lista video z parzystych sezonów: ");
        List<Video> seasonVideos = seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .collect(Collectors.toList());
        System.out.println(seasonVideos);

        System.out.println("Lista video z parzystych sezonów i epizodów: ");
        List<Video> seasonEpisodesVideos = seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 == 0)
                .flatMap((Episode e) -> e.videos.stream())
                .collect(Collectors.toList());
        System.out.println(seasonEpisodesVideos);

        System.out.println("List video typu clip z parzystych episodów i nieparzystych sezonów: ");
        List<Video> videoClips = seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 != 0)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 == 0)
                .flatMap((Episode e) -> e.videos.stream())
                //.filter((Video v) -> v.videoType.equals(VideoType.CLIP))
                .filter((Video v) -> v.videoType == VideoType.CLIP)
                .collect(Collectors.toList());
        System.out.println(videoClips);

        System.out.println("List video typu preview z nieparzystych episodów i parzystych sezonów: ");
        List<Video> previewVideo = seasons.stream()
                .filter((Season s) -> s.seasonNumber % 2 == 0)
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.episodeNumber % 2 != 0)
                .flatMap((Episode e) -> e.videos.stream())
                .filter((Video v) -> v.videoType == VideoType.PREVIEW)
                .collect(Collectors.toList());
        System.out.println(previewVideo);

        /*
        1. Znajdź dowolny epizod z sezonu 3 i jeśli taki istnieje wyświetl go
2. Znajdź dowolne video typu Preview i jeśli takie istnieje wyświetl informację o jego nazwie
3. Znajdź dowolny sezon z conajmniej 3 epizodami i jeśli taki istnieje zwróć go, jeśli natomiast nie istnieje zwróć domyślny sezon
4. Wyrzuć wyjątek jeśli video które jest z sezonu 3 i parzystego epizodu nie istnieje
5. Znajdź dowolny epizod z sezonu który zawiera ich co najmniej 10, jeśli istnieje zwróć go, jeśli nie zwróć domyślny
6. Znajdź dowolny epizod który zawiera conajmniej dwa video i jeśli istnieje zwróć go, jeśli nie to zwróć domyślny epizod z losowo wybranym numerem epizodu
         */
        System.out.println("Znajdź dowolny epizod z sezonu 3 i jeśli taki istnieje wyświetl go ");
        seasons.stream()
                .filter((Season s) -> s.seasonNumber == 3)
                .flatMap((Season s) -> s.episodes.stream())
                .findAny()
                .ifPresent((Episode e) -> {
                    System.out.println("Season 3 episode: " + e);
                });

        System.out.println("Znajdź dowolne video typu Preview i jeśli takie istnieje wyświetl informację o jego nazwie");

        seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .flatMap((Episode e) -> e.videos.stream())
                .filter((Video v) -> v.videoType == VideoType.PREVIEW)
                .findAny()
                .ifPresent((Video v) -> System.out.println("Preview video: " + v));

        System.out.println("Znajdź dowolny sezon z conajmniej 3 epizodami i jeśli taki istnieje zwróć go, jeśli natomiast nie istnieje zwróć domyślny sezon");

        Season seasonWith3Episodes = seasons.stream()
                .filter((Season s) -> s.episodes.size() >= 3)
                .findAny()
                .orElse(new Season("", -1, new ArrayList<>()));
        System.out.println("Season with at least 3 episodes: " + seasonWith3Episodes);


        System.out.println("Wyrzuć wyjątek jeśli video które jest z sezonu 3 i parzystego epizodu nie istnieje");
//        seasons.stream()
//                .filter((Season s) -> s.seasonNumber == 3)
//                .flatMap((Season s) -> s.episodes.stream())
//                .filter((Episode e) -> e.episodeNumber % 2 == 0)
//                .flatMap((Episode e) -> e.videos.stream())
//                .findAny()
//                .orElseThrow(()->new NoSuchElementException("no video from season 3 found!"));


        System.out.println("Znajdź dowolny epizod z sezonu który zawiera ich co najmniej 10, jeśli istnieje zwróć go, jeśli nie zwróć domyślny");
        Episode episodeFromSeasonWith10Episodes = seasons.stream()
                .filter((Season s) -> s.episodes.size() >= 10)
                .flatMap((Season s) -> s.episodes.stream())
                .findAny()
                .orElse(new Episode("", -1, new ArrayList<>()));
        System.out.println(episodeFromSeasonWith10Episodes);


        System.out.println("Znajdź dowolny epizod który zawiera conajmniej dwa video i jeśli istnieje zwróć go, jeśli nie to zwróć domyślny epizod z losowo wybranym numerem epizodu");
        Episode episodeWith2Videos = seasons.stream()
                .flatMap((Season s) -> s.episodes.stream())
                .filter((Episode e) -> e.videos.size() >= 2)
                .findAny()
                .orElseGet(()-> {
                    Random random = new Random();
                    int episodeNumber = random.nextInt();
                    return new Episode("", episodeNumber, new ArrayList<>());
                });
        System.out.println(episodeWith2Videos);
    }

}
