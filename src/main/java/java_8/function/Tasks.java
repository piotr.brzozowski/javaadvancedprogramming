package java_8.function;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

public class Tasks {

    public static void main(String[] args) {
        //Function - konwersja obiektów
        //ZAD 1
        System.out.println("ZAD 1:");
        Function<String, Account> toAccount = (String accountName) -> new Account(accountName);
        Account account = toAccount.apply("MBANK");
        System.out.println(account);

        //ZAD 2
        System.out.println("ZAD 2:");
        Function<Integer, Box> toBox = (Integer secret) -> new Box(secret);
        Box box = toBox.apply(235);
        System.out.println(box);

        //Supplier -> dostarczanie danych
        //ZAD 3
        System.out.println("ZAD 3: ");
        Supplier<LocalDate> getCurrentDate = () -> LocalDate.now();
        System.out.println(getCurrentDate.get());

        //ZAD 4
        System.out.println("ZAD 4: ");
        Supplier<String> getFromattedDate = () -> {
            LocalDate currentDate = LocalDate.now();
            String result = currentDate.format(DateTimeFormatter.ofPattern("dd, MM, yyyy"));
            return result;
        };
        System.out.println(getFromattedDate.get());

        //Consumer -> wyświetlanie podanych danych
        //ZAD 5
        System.out.println("ZAD 5: ");
        User user = new User("wacław", "nowak", 20);
        Consumer<User> toUpperCaseFormatter = (User userObj) -> {
            userObj.name = userObj.name.toUpperCase();
            userObj.lastName = userObj.lastName.toUpperCase();
            System.out.println(userObj);
        };
        toUpperCaseFormatter.accept(user);

        //ZAD 6
        System.out.println("ZAD 6: ");
        Consumer<User> userDisplay = (User userObj) -> {
            System.out.println("***************");
            System.out.println("Imię: " + userObj.name);
            System.out.println("Nazwisko: " + userObj.lastName);
            System.out.println("Wiek: " + userObj.age);
            System.out.println("***************");
        };
        userDisplay.accept(user);

        //Predicate -> sprawdzanie poprawności przekazanych danych
        //ZAD 7
        System.out.println("ZAD 7: ");
        Predicate<LocalDate> isLeapYear = (LocalDate localDate) -> localDate.isLeapYear();
        boolean result = isLeapYear.test(LocalDate.now());
        System.out.println("Is leap year: " + result);

        //ZAD 8
        System.out.println("ZAD 8: ");
        String phoneNumber = "+48333445";
        Predicate<String> isPhoneNumberValid = (String number) -> number.startsWith("+48") && number.length() == 12;
        boolean phoneNumberValidationResult = isPhoneNumberValid.test(phoneNumber);
        System.out.println("Is phone number valid: " + phoneNumberValidationResult);
    }
}

class User {
    String name;
    String lastName;
    int age;

    public User(String name, String lastName, int age) {
        this.name = name;
        this.lastName = lastName;
        this.age = age;
    }

    @Override
    public String toString() {
        return "User{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", age=" + age +
                '}';
    }
}

class Account {
    public String name;
    public Account(String name){
        this.name=name;
    }

    @Override
    public String toString() {
        return "Account{" +
                "name='" + name + '\'' +
                '}';
    }
}

class Box {
    public Integer value;
    public Box(Integer value){
        this.value=value;
    }

    @Override
    public String toString() {
        return "Box{" +
                "value=" + value +
                '}';
    }
}
