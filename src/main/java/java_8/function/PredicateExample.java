package java_8.function;

import java.util.function.Predicate;

public class PredicateExample {

    public static void main(String[] args) {
        Predicate<Integer> isEven = (Integer value) -> value % 2 == 0;
        System.out.println("Czy liczba 8 jest parzysta: " + isEven.test(8));
        System.out.println("Czy liczba 9 jest parzysta: " + isEven.test(9));

        User user = new User(null);
        User user1 = new User("Piotr");
        Predicate<User> isUserNameValid = (User userObj) -> userObj.name != null;
        System.out.println("Czy nazwa użytkownika jest poprawna: " + isUserNameValid.test(user));
        System.out.println("Czy nazwa użytkownika jest poprawna: " + isUserNameValid.test(user1));
    }

    static class User {
        String name;

        public User(String name) {
            this.name = name;
        }
    }
}
