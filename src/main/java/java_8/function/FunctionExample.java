package java_8.function;

import java.util.function.Function;

public class FunctionExample {

    public static void main(String[] args) {
        Function<Integer, String> toStr = integer -> integer + "->STR";
        System.out.println(toStr.apply(10));

        Car car = new Car("test", "test");
        //[name: test, type: test]
//        Function<Car, String> carToString = (Car carObj) -> {
//            return "[name: " + carObj.name + ", type: " + carObj.type + "]";
//        };
        //Function<Car, String> carToString = (carObj) -> "[name: " + carObj.name + ", type: " + carObj.type + "]";
        Function<Car, String> carToString = carObj -> "[name: " + carObj.name + ", type: " + carObj.type + "]";
        String result = carToString.apply(car);
        System.out.println(result);
    }

    static class Car {
        String name;
        String type;

        public Car(String name, String type) {
            this.name = name;
            this.type = type;
        }
    }
}
