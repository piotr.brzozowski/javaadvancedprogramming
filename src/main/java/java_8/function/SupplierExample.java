package java_8.function;

import java.util.Random;
import java.util.function.Supplier;

public class SupplierExample {

    public static void main(String[] args) {
        System.out.println("Hello World supplier -> ");
        Supplier<String> helloWorldSupplier = () -> {
            return "Hello World!";
        };

        System.out.println(helloWorldSupplier.get());

        System.out.println("Random value supplier -> ");
        Supplier<Integer> randomValueSupplier = () -> {
            Random random = new Random();
            return random.nextInt(100);
        };
        System.out.println(randomValueSupplier.get());
        System.out.println(randomValueSupplier.get());
        System.out.println(randomValueSupplier.get());

        //typ T -> klasa dziedzicząca po klasie Object, int -> typ prymitywny, nie obiektowy
    }
}
