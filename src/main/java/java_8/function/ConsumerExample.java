package java_8.function;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;

public class ConsumerExample {

    public static void main(String[] args) {
        Car car = new Car("test", "test");
        //wyświetli obiekt w postaci tekstowe
        Consumer<Car> carDisplay = (Car carObj) -> {
            System.out.println("#############################");
            System.out.println("Name: " + carObj.name);
            System.out.println("Type: " + carObj.type);
            System.out.println("#############################");
        };
        carDisplay.accept(car);

        System.out.println("Wyświetlanie listy liczb: ");
        List<Integer> values = Arrays.asList(2, 4, 6, 8, 10);
        Consumer<List<Integer>> listDisplay = (List<Integer> list) -> {
            for (int i =0;i<list.size();i++) {
                System.out.println(list.get(i));
            }
        };
        listDisplay.accept(values);
    }

    static class Car {
        String name;
        String type;

        public Car(String name, String type) {
            this.name = name;
            this.type = type;
        }
    }
}
