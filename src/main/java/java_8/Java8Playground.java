package java_8;

public class Java8Playground {

    public static void main(String[] args) {
        //implementacja klasowa
        //obiekt klasy MathOperation możemy stworzyć w dowolnym miejscu w kodzie
        System.out.println("Class implementation -> ");
        MathOperation addMathOperation = new AddMathOperation();
        doCalculate(addMathOperation, 10, 20);

        //klasa anonimowa v1
        //obiekt klasy MathOperation został stworzony w metodzie main i może być
        // wykorzystany tylko w metodzie main i metodach wywoływanych z metody main
        System.out.println("Anonymous class v1 -> ");
//        MathOperation multiplyMathOperation = new MathOperation() {
//            @Override
//            public int calculate(int a, int b) {
//                return a * b;
//            }
//        };
        //wyrażdnie lambda
//        MathOperation multiplyMathOperation = (int a, int b) -> {
//            return a*b;
//        };
        MathOperation multiplyMathOperation = (a, b) -> a*b;

        //klasa anonimowa v2
        System.out.println("Anonymous class v2 -> ");
//        MathOperation divideMathOperation = new MathOperation() {
//            @Override
//            public int calculate(int a, int b) {
//                return a/b;
//            }
//        };
//        MathOperation divideMathOperation = (int a, int b) -> {
//            return a/b;
//        };
        MathOperation divideMathOperation = (a, b) -> a/b;
        //w 33 lini mam dostęp do operacji dzielenia

        //klasa anonimowa v3
        System.out.println("Anonymous class v3 -> ");
        //obiekt klasy MathOperation został stworzony na rzecz wywołania metody doCalculate i może być
        // wykorzystany tylko w metodzie doCalculate i metodach wywoływanych z metody doCalculate
//        doCalculate(new MathOperation() {
//            @Override
//            public int calculate(int a, int b) {
//                return a-b;
//            }
//        }, 10, 20);
        //wyrażenie lambda


        doCalculate(addMathOperation, 10, 20);


        doCalculate(multiplyMathOperation, 10, 20);


        doCalculate(divideMathOperation, 10, 20);


        doCalculate((int a, int b) -> {
            System.out.println("test");
            return a - b;
        }, 10, 20);
        //w lini 44 nie mam dostępu do operacji odejmowania
    }

    public static void doCalculate(MathOperation mathOperation, int a, int b) {
        int result = mathOperation.calculate(a, b);
        System.out.println("Result: " + result);
    }
}
