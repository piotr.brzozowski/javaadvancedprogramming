package java_8;

@FunctionalInterface
public interface MathOperation {
    int calculate(int a, int b);

    default int calculate(int a) {
        return a;
    }

    default int calculate() {
        return 0;
    }

    static int value = 0;

    static void test() {
        System.out.println();
    }
}
