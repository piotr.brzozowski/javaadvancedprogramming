package facade.example1;

//biblioteka nr 2
public interface AdsPlayer {
    void start();
    void play();
    void pause();
}
