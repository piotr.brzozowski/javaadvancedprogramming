package facade.example1;

//biblioteka nr 1
public interface VideoPlayer {
    void star(String videoId);
    void play();
    void pause();
}
