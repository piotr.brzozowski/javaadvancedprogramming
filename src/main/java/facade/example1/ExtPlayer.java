package facade.example1;

//Fasada łącząca działanie bilbioteki nr 1 (VideoPlayer) i bibliotki nr 2 (AdsPlayer)
public class ExtPlayer {
    private VideoPlayer videoPlayer;
    private AdsPlayer adsPlayer;
    private boolean isAdsPlaying = false;

    public ExtPlayer(VideoPlayer videoPlayer, AdsPlayer adsPlayer) {
        this.videoPlayer = videoPlayer;
        this.adsPlayer = adsPlayer;
    }

    public void start(String videoId) {
        isAdsPlaying = true;
        adsPlayer.start();

        videoPlayer.star(videoId);
        isAdsPlaying = false;
    }

    public void pause() {
        if (isAdsPlaying) {
            adsPlayer.pause();
        } else {
            videoPlayer.pause();
        }
    }

    public void play() {
        if (isAdsPlaying) {
            adsPlayer.play();
        } else {
            videoPlayer.play();
        }
    }
}
