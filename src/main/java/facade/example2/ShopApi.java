package facade.example2;

public class ShopApi {

    private final PaymentApi paymentApi = new PaymentApi();
    private final UserApi userApi = new UserApi();
    private final ProductApi productApi = new ProductApi();
    private boolean isUserLoggedIn = false;

    public void register(String login, String password) {
        userApi.register(login, password);
        System.out.println("User registered: " + login + ", " + password);
    }

    public void login(String login, String password) {
        isUserLoggedIn = userApi.login(login, password);
        System.out.println("Is user logged in: " + isUserLoggedIn);
    }

    public void logout() {
        if (isUserLoggedIn) {
            userApi.logout();
            isUserLoggedIn = false;
            System.out.println("User logged out!");
        }
    }

    public void buyProduct(Product product) {
        if (!isUserLoggedIn) {
            System.out.println("Sorry! You can't buy product as anonymous user!");
        }
        if (productApi.getProductByName(product.getName()) != null){
            paymentApi.pay(product.getPrice());
            removeProduct(product.getName());
        }
    }

    public void addProduct(Product product) {
        productApi.addProduct(product);
    }

    public void removeProduct(String productName) {
        productApi.removeProduct(productName);
    }
}
