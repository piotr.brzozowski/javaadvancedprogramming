package facade.example2;

public class PaymentApi {

    public void pay(float amount) {
        System.out.println("Conecting to bank ....");
        try {
            //4000 ms -> 4s
            Thread.sleep(4000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Payment succesfull!");
    }
}
