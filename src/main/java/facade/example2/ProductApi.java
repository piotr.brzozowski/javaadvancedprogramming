package facade.example2;

import java.util.ArrayList;
import java.util.List;

public class ProductApi {

    private final List<Product> products = new ArrayList<>();

    public List<Product> getProducts() {
        return products;
    }

    public Product getProductByName(String name) {
        for (Product product : products) {
            if (product.getName().equals(name)) {
                return product;
            }
        }
        return null;
    }

    public void addProduct(Product product) {
        products.add(product);
    }

    public void removeProduct(String productName) {
        for (Product product : products) {
            if (product.getName().equals(productName)) {
                products.remove(product);
                return;
            }
        }
    }

}
