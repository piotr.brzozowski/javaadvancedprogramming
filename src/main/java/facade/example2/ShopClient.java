package facade.example2;

public class ShopClient {

    public static void main(String[] args) {
        ShopApi shopApi = new ShopApi();

        shopApi.addProduct(new Product("Iphone", "5 gen", 1000f));
        shopApi.addProduct(new Product("Samsung", "21", 800f));

        shopApi.register("XYZ", "XYZ");
        shopApi.login("XYZ", "XYZ");
        shopApi.buyProduct(new Product("Iphone", "5 gen", 1000f));
        shopApi.logout();
    }
}
