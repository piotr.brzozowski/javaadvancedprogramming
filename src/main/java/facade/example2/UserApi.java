package facade.example2;

public class UserApi {

    private String login;
    private String password;

    public void register(String login, String password) {
        this.login = login;
        this.password = password;
    }

    public boolean login(String login, String password) {
        if (this.login != null &&
                this.password != null &&
                this.login.equals(login) &&
                this.password.equals(password)) {
            return true;
        }
        return false;
    }

    public void logout() {
        login = null;
        password = null;
    }
}
