package prototype.example2.deep_copy;

public class Movie implements Cloneable {
    private String title;
    private int yearOfRelease;
    private Director director;

    public Movie(String title, int yearOfRelease, Director director) {
        this.title = title;
        this.yearOfRelease = yearOfRelease;
        this.director = director;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Movie movieClone = (Movie) super.clone();
        Director directorClone = (Director) this.director.clone();
        movieClone.director = directorClone;
        return movieClone;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getYearOfRelease() {
        return yearOfRelease;
    }

    public void setYearOfRelease(int yearOfRelease) {
        this.yearOfRelease = yearOfRelease;
    }

    public Director getDirector() {
        return director;
    }

    public void setDirector(Director director) {
        this.director = director;
    }

    @Override
    public String toString() {
        return "Movie{" +
                "title='" + title + '\'' +
                ", yearOfRelease=" + yearOfRelease +
                ", director=" + director +
                '}';
    }
}
