package prototype.example2.shallow_copy;

public class Director {
    public String name;
    public String lastName;
    public String movieType;

    public Director(String name, String lastName, String movieType) {
        this.name = name;
        this.lastName = lastName;
        this.movieType = movieType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMovieType() {
        return movieType;
    }

    public void setMovieType(String movieType) {
        this.movieType = movieType;
    }

    @Override
    public String toString() {
        return "Director{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", movieType='" + movieType + '\'' +
                '}';
    }
}
