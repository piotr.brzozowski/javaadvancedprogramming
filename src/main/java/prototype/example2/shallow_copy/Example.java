package prototype.example2.shallow_copy;

public class Example {

    public static void main(String[] args) {
        Director director = new Director("J.J", "Abrams", "Action");
        Movie movie = new Movie("Star Wars Force Awaken", 2015, director);
        Movie movieClone = null;
        try {
            movieClone = (Movie) movie.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("BEFORE");
        System.out.println(movie);
        System.out.println(movieClone);

        System.out.println("AFTER");
        movieClone.setTitle("Star Wars Last Jedi");
        movieClone.setYearOfRelease(2017);
        movieClone.getDirector().name = "Rayan";
        movie.getDirector().lastName = "Johnson";
        System.out.println(movie);
        System.out.println(movieClone);
    }
}
