package prototype.example2.extended;

public class MovieType implements Cloneable {
    public String type;
    public int minimumRecommendedAge;

    public MovieType(String type, int minimumRecommendedAge) {
        this.type = type;
        this.minimumRecommendedAge = minimumRecommendedAge;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public String toString() {
        return "MovieType{" +
                "type='" + type + '\'' +
                ", minimumRecommendedAge=" + minimumRecommendedAge +
                '}';
    }
}
