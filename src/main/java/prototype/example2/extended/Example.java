package prototype.example2.extended;

public class Example {

    public static void main(String[] args) {
        MovieType movieType = new MovieType("Action", 13);
        Director director = new Director("J.J", "Abrams", movieType);
        Movie movie = new Movie("Star Wars Force Awaken", 2015, director);
        Movie movieClone = null;
        try {
            movieClone = (Movie) movie.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("BEFORE");
        System.out.println(movie);
        System.out.println(movieClone);

        System.out.println("AFTER");
        movieClone.setTitle("Star Wars Last Jedi");
        movieClone.setYearOfRelease(2017);
        movieClone.getDirector().name = "Rayan";
        movieClone.getDirector().lastName = "Johnson";
        movieClone.getDirector().movieType.type = "Animation";
        System.out.println(movie);
        System.out.println(movieClone);
    }
}
