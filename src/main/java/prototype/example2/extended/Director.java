package prototype.example2.extended;

public class Director implements Cloneable {
    public String name;
    public String lastName;
    public MovieType movieType;

    public Director(String name, String lastName, MovieType movieType) {
        this.name = name;
        this.lastName = lastName;
        this.movieType = movieType;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Director directorClone = (Director) super.clone();
        MovieType movieTypeClone = (MovieType) this.movieType.clone();
        directorClone.movieType = movieTypeClone;
        return directorClone;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public MovieType getMovieType() {
        return movieType;
    }

    public void setMovieType(MovieType movieType) {
        this.movieType = movieType;
    }

    @Override
    public String toString() {
        return "Director{" +
                "name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", movieType='" + movieType + '\'' +
                '}';
    }
}
