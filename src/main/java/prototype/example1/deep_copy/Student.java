package prototype.example1.deep_copy;

public class Student implements Cloneable {
    int id;
    String name;
    Course course;

    public Student(int id, String name, Course course) {
        this.id = id;
        this.name = name;
        this.course = course;
    }

    @Override
    protected Object clone() throws CloneNotSupportedException {
        Student studentClone = (Student) super.clone();//shallow copy, kurs jest ten sam dla dwóch obiektów
        Course courseClone = (Course) studentClone.course.clone();//klonowanie obiektu course w celu stworzenia niezależnego obiektu
        studentClone.course = courseClone;//zmieniamy referencję obiektu studentClone by wskazywał na sklonowany obiekt a nie na obiekt this.course
        return studentClone;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", course=" + course +
                '}';
    }
}
