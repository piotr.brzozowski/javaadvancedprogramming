package prototype.example1.shallow_copy;

public class ShallowCopyExample {

    public static void main(String[] args) {
        Course course = new Course("math", "biology", "computer science");
        Student student = new Student(1, "John", course);
        Student studentClone = null;

        try {
            studentClone = (Student) student.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        System.out.println("Before change: ");
        System.out.println("St: " + student);
        System.out.println("St1: " + studentClone);

        System.out.println("After change: ");
        studentClone.name = "Sam";
        studentClone.course.subject1 = "science";
        System.out.println("St: " + student);
        System.out.println("St1: " + studentClone);
    }
}
