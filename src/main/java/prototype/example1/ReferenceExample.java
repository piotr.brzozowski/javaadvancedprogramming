package prototype.example1;

public class ReferenceExample {

    public static void main(String[] args) {
        Car car = new Car("Porsche", "Sport");
        Car car1 = car;
        Car car2 = new Car("TEst", "test");
        car1.setName("Ferrari");
        System.out.println("Car: " + car.getName());//Ferrari
        System.out.println("Car1: " + car1.getName());//Ferrari

        doSomething(car);//Car carReference = car;
        doSomething(car2);//Car carReference = car2;
    }

    static void doSomething(Car carReference) {
        System.out.println(carReference);
    }
}
