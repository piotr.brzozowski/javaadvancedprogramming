package prototype.example1;

public class CopyExample {

    public static void main(String[] args) {
        Car car = new Car("Test", "Test");
        try {
            Car car1 = (Car) car.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
    }
}
