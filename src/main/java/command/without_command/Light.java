package command.without_command;

public class Light {
    private String id;
    private boolean on;

    public Light(String id) {
        this.id = id;
    }

    public void switchLight(boolean on) {
        this.on = on;
    }

    public boolean isOn() {
        return on;
    }

    public String getId() {
        return id;
    }
}