package command.without_command;

public class Person {

    public static void main(String[] args) {
        RemoteControllerWithouthCommand remoteController = new RemoteControllerWithouthCommand();
        Light lightBedroom = new Light("Bedroom");
        Light lightKitchen = new Light("Kitchen");
        Light lightToilet = new Light("Toilet");

        System.out.println("Idziemy do pokoju");
        remoteController.setLight(lightBedroom);
        remoteController.turnOn();

        System.out.println("Idziemy do kuchni");
        remoteController.turnOff();
        remoteController.setLight(lightKitchen);
        remoteController.turnOn();

        System.out.println("Idziemy do toalety (zapomnieliśmy zgasić światła)");
        remoteController.setLight(lightToilet);
        remoteController.turnOff();

    }
}
