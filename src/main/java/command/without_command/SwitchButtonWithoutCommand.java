package command.without_command;

//Invoker
public class SwitchButtonWithoutCommand {
    private Light light;

    public void setLight(Light light) {
        this.light = light;
    }

    //wykonaj polecenie
    public void switchClick() {
        if (light.isOn()) {
            System.out.println("Light: " + light.getId());
            if (light.isOn()) {
                System.out.println("Light is turned on already!");
                return;
            }
            System.out.println("Light turning on.....");
            light.switchLight(true);
            System.out.println("Light turned on!");
        }else {
            System.out.println("Light: " + light.getId());
            if (!light.isOn()) {
                System.out.println("Light is turned off already!");
                return;
            }
            System.out.println("Light turning off.....");
            light.switchLight(false);
            System.out.println("Light turned off!");
        }
    }
}
