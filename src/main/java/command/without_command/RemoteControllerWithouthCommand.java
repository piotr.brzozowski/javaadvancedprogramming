package command.without_command;


//Invoker
public class RemoteControllerWithouthCommand {
    private Light light;

    public void setLight(Light light) {
        this.light = light;
    }

    //wykonaj polecenie
    public void turnOn() {
        System.out.println("Light: " + light.getId());
        if (light.isOn()) {
            System.out.println("Light is turned on already!");
            return;
        }
        System.out.println("Light turning on.....");
        light.switchLight(true);
        System.out.println("Light turned on!");
    }

    //wykonaj polecenie
    public void turnOff() {
        System.out.println("Light: " + light.getId());
        if (!light.isOn()) {
            String test = "tst";
            System.out.println("Light is turned off already!");
            return;
        }
        System.out.println("Light turning off.....");
        light.switchLight(false);
        System.out.println("Light turned off!");
    }
}

