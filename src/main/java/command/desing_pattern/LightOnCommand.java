package command.desing_pattern;

public class LightOnCommand implements Command {

    private final Light light;

    public LightOnCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        System.out.println("Light: " + light.getId());
        if (light.isOn()) {
            System.out.println("Light is turned on already!");
            return;
        }
        System.out.println("Light turning on.....");
        light.switchLight(true);
        System.out.println("Light turned on!");
    }
}
