package command.desing_pattern;

public class ChangeLightColorCommand implements Command {

    private Light light;
    private String color;

    public ChangeLightColorCommand(String color, Light light) {
        this.light = light;
        this.color = color;
    }

    @Override
    public void execute() {
        light.setColor(color);
        System.out.println("Change color: " + color);
    }
}
