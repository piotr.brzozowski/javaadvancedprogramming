package command.desing_pattern;

//Invoker
public class SwitchButton {
    private Light light;
    private Command lightOff;
    private Command lightOn;

    public void setLight(Light light) {
        this.light = light;
        lightOff = new LightOffCommand(light);
        lightOn = new LightOnCommand(light);
    }

    //wykonaj polecenie
    public void switchClick() {
        if (light.isOn()) {
            lightOff.execute();
        }else {
            lightOn.execute();
        }
    }
}
