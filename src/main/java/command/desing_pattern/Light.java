package command.desing_pattern;

public class Light {
    private String id;
    private boolean on;
    private String color = "yellow";

    public Light(String id) {
        this.id = id;
    }

    public void switchLight(boolean on) {
        this.on = on;
    }

    public boolean isOn() {
        return on;
    }

    public String getId() {
        return id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}