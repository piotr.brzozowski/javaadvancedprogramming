package command.desing_pattern;

public class Person {

    public static void main(String[] args) {
        RemoteController remoteController = new RemoteController();
        Light lightBedroom = new Light("Bedroom");
        Light lightKitchen = new Light("Kitchen");
        Light lightToilet = new Light("Toilet");

        System.out.println("Idziemy do pokoju");
        remoteController.setLight(lightBedroom);
        remoteController.turnOn();
        remoteController.changeColor("green");

        System.out.println("Idziemy do kuchni");
        remoteController.turnOff();
        remoteController.setLight(lightKitchen);
        remoteController.turnOn();

        System.out.println("Idziemy do toalety (zapomnieliśmy zgasić światła)");
        remoteController.setLight(lightToilet);
        remoteController.turnOff();

    }
}
