package command.desing_pattern;

//Invoker
public class RemoteController {
    private Light light;

    public void setLight(Light light) {
        this.light = light;
    }

    //wykonaj polecenie
    public void turnOn() {
        new LightOnCommand(light).execute();
    }

    //wykonaj polecenie
    public void turnOff() {
        new LightOffCommand(light).execute();
    }

    public void changeColor(String color) {
        new ChangeLightColorCommand(color, light).execute();
    }
}

