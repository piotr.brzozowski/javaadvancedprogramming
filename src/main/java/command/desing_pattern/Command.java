package command.desing_pattern;

public interface Command {
    //wykonaj coś na receiver -> Light
    void execute();
}
