package command.desing_pattern;

public class LightOffCommand implements Command {

    private final Light light;

    public LightOffCommand(Light light) {
        this.light = light;
    }

    @Override
    public void execute() {
        System.out.println("Light: " + light.getId());
        if (!light.isOn()) {
            System.out.println("Light is turned off already!");
            return;
        }
        System.out.println("Light turning off.....");
        light.switchLight(false);
        System.out.println("Light turned off!");
    }
}
