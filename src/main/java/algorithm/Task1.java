package algorithm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/*
1. Znajdź pierwszy powtarzający się charakter w ciągu znaków
- ABCA
- BCABA
- ABC
 */
public class Task1 {

    public static void main(String[] args) {
        String input = "ABEDCC";
        solution1(input);
        solution2(input);
        solution3(input);
    }

    public static void solution1(String input) {
        long starTime = System.nanoTime();
        for (int i=0;i<input.length();i++) {
            for(int j=i+1;j<input.length();j++) {
                if (input.charAt(i) == input.charAt(j)){
                    System.out.println("solution1: " + input.charAt(i));
                    System.out.println(System.nanoTime() - starTime);
                    return;
                }
            }
        }
        System.out.println(System.nanoTime() - starTime);
        System.out.println("Brak");
    }

    public static void solution2(String input) {
        Set<Character> characters = new HashSet<>();
        for (int i=0;i<input.length();i++) {
            if (characters.contains(input.charAt(i))){
                System.out.println("solution2: " + input.charAt(i));
                return;
            }
            characters.add(input.charAt(i));
        }
        System.out.println("BRAK");
    }

    public static void solution3(String input) {
        Map<Character, Integer> map = new HashMap<>();
        for (int i=0;i<input.length();i++) {
            if (map.containsKey(input.charAt(i))){
                System.out.println("solution3: " + input.charAt(i));
                return;
            }
            map.put(input.charAt(i), 1);
        }
        System.out.println("BRAK");
    }
}
