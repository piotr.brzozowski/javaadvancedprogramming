package iterator.book;

//Iterator
public interface BookIteratorApi {
    Book next();
    boolean hasNext();
}
