package iterator.book;

import java.util.List;

//ConcreteIterator
public class BookIterator implements BookIteratorApi {

    private List<Book> books;
    private int currentElementIndex = 0;

    public BookIterator(List<Book> books) {
        this.books = books;
    }

    public Book next() {
        if (hasNext()) {
            return books.get(currentElementIndex++);
        }
        return null;
    }

    public boolean hasNext() {
        return books.size() > currentElementIndex;
    }
}
