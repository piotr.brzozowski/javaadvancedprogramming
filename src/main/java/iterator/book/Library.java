package iterator.book;

import java.util.ArrayList;
import java.util.List;

//zbiór książek
//ConcreteAggregate
public class Library {
    //dowolna struktura danych
    private List<Book> books = new ArrayList<Book>();

    public void addBook(Book book) {
        this.books.add(book);
    }

    //tworzenie mechanizmu do poruszania się po bibliotece
    public BookIteratorApi createIterator() {
        return new BookIterator(books);
    }
}
