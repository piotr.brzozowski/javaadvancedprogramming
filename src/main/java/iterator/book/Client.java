package iterator.book;

//Client
public class Client {

    public static void main(String[] args) {
        Library library = new Library();
        library.addBook(new Book("1", "Star Wars", "Action"));
        library.addBook(new Book("2", "Harry Potter", "Fantasy"));
        library.addBook(new Book("3", "Zbrodnia i Kara", "Drama"));

        BookIteratorApi bookIteratorApi = library.createIterator();
        System.out.println("Poruszanie się po książkach z biblioteki");
        while (bookIteratorApi.hasNext()) {
            Book bookFromLibrary = bookIteratorApi.next();
            System.out.println(bookFromLibrary);
        }
    }
}
