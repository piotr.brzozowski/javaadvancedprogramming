package iterator.football.v1;

import java.util.List;

public class FootballPlayerIterator implements FootballPlayerIteratorApi {

    private List<FootballPlayer> footballPlayers;
    private int currentElementIndex = 0;

    public FootballPlayerIterator(List<FootballPlayer> footballPlayers) {
        this.footballPlayers = footballPlayers;
    }

    @Override
    public FootballPlayer next() {
        if (hasNext()) {
            return footballPlayers.get(currentElementIndex++);
        }
        return null;
    }

    @Override
    public boolean hasNext() {
        return footballPlayers.size() > currentElementIndex;
    }
}
