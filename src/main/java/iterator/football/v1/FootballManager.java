package iterator.football.v1;

public class FootballManager {

    public static void main(String[] args) {
        FootballTeam footballTeam = new FootballTeam();
        footballTeam.addFootballPlayer(new FootballPlayer("Cristiano", "Ronaldo", 7));
        footballTeam.addFootballPlayer(new FootballPlayer("Robert", "Lewandowski", 9));
        footballTeam.addFootballPlayer(new FootballPlayer("Kamil", "Grosicki", 11));

        FootballPlayerIteratorApi iteratorApi = footballTeam.createIterator();
        System.out.println("Piłkarze w zespole");
        while (iteratorApi.hasNext()) {
            System.out.println(iteratorApi.next());
        }

        //powinniśmy unikać możliwości tworzenia takie obiektu bez powiązania z agregatem (FootballTeam)
        //FootballPlayerIterator footballPlayerIterator = new FootballPlayerIterator(Collections.emptyList());
    }
}
