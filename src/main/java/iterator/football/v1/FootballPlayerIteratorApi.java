package iterator.football.v1;

public interface FootballPlayerIteratorApi {
    FootballPlayer next();
    boolean hasNext();
}
