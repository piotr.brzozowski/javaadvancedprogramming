package iterator.football.v1;

import java.util.ArrayList;
import java.util.List;

public class FootballTeam {

    private List<FootballPlayer> footballPlayers = new ArrayList<>();

    public void addFootballPlayer(FootballPlayer footballPlayer) {
        footballPlayers.add(footballPlayer);
    }

    public FootballPlayerIteratorApi createIterator() {
        return new FootballPlayerIterator(footballPlayers);
    }
}
