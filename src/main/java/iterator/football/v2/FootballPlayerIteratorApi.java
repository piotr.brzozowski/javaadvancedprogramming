package iterator.football.v2;

public interface FootballPlayerIteratorApi {
    FootballPlayer next();
    boolean hasNext();
    FootballPlayer prev();
    boolean hasPrev();
}
