package iterator.football.v2;

import java.util.ArrayList;
import java.util.List;

public class FootballTeam {

    private List<FootballPlayer> footballPlayers = new ArrayList<>();

    public void addFootballPlayer(FootballPlayer footballPlayer) {
        footballPlayers.add(footballPlayer);
    }

    public FootballPlayerIteratorApi createIterator() {
        return new FootballPlayerIterator(footballPlayers);
    }

    //klasa FootballPlayerIterator może być wykorzystywana w klasie nadrzędnej FootballTeam, po za tą klasą, klasa ta będzie w całości niewidoczna
    /*
    jeśli klasa private -> to można stworzyć w obiekt w klasie FootballTeam, ale nigdzie więcej, klasa nie będzie widoczna poza FootballTeam
    jeśli klasa public -> to można stworzyć w obiekt w klasie FootballTeam,  klasa będzie widoczna poza FootballTeam, ale poza FootballTeam nie można stworzyć jej obiektu
     jeśli klasa static public -> to można stworzyć w obiekt w klasie FootballTeam,  klasa będzie widoczna poza FootballTeam, poza FootballTeam można stworzyć jej obiektu
     */
    private class FootballPlayerIterator implements FootballPlayerIteratorApi {

        private List<FootballPlayer> footballPlayers;
        private int currentElementIndex = 0;

        public FootballPlayerIterator(List<FootballPlayer> footballPlayers) {
            this.footballPlayers = footballPlayers;
        }

        @Override
        public FootballPlayer next() {
            if (hasNext()) {
                return footballPlayers.get(currentElementIndex++);
            }
            return null;
        }

        @Override
        public boolean hasNext() {
            return footballPlayers.size() > currentElementIndex;
        }

        @Override
        public FootballPlayer prev() {
            if (hasPrev()) {
                return footballPlayers.get(--currentElementIndex);
            }
            return null;
        }

        @Override
        public boolean hasPrev() {
            return currentElementIndex > 0;
        }
    }
}
