package iterator.football.v2;

import java.util.Collections;

public class FootballManager {

    public static void main(String[] args) {
        FootballTeam footballTeam = new FootballTeam();
        footballTeam.addFootballPlayer(new FootballPlayer("Cristiano", "Ronaldo", 7));
        footballTeam.addFootballPlayer(new FootballPlayer("Robert", "Lewandowski", 9));
        footballTeam.addFootballPlayer(new FootballPlayer("Kamil", "Grosicki", 11));

        FootballPlayerIteratorApi iteratorApi = footballTeam.createIterator();
        System.out.println("Piłkarze w zespole");
        System.out.println("NEXT: ");
        while (iteratorApi.hasNext()) {
            System.out.println(iteratorApi.next());
        }
        System.out.println("PREV: ");
        while (iteratorApi.hasPrev()) {
            System.out.println(iteratorApi.prev());
        }

        //FootballTeam.FootballPlayerIterator footballPlayerIterator = new FootballTeam.FootballPlayerIterator();
        //powinniśmy unikać możliwości tworzenia takie obiektu bez powiązania z agregatem (FootballTeam)
        //FootballPlayerIterator footballPlayerIterator = new FootballPlayerIterator(Collections.emptyList());
    }
}
